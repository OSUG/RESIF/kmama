#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import OrderedDict
from subprocess import run as s_run
import click
from click_option_group import (
    optgroup,
    RequiredMutuallyExclusiveOptionGroup,
    MutuallyExclusiveOptionGroup,
)
try:
    from kmama.utilities import cprint
    from kmama.core.generic import CLI
    from kmama.core.applications import main_runner
    from kmama.core.volumes import main_volume
    from kmama.core.check import main_check
except ImportError:
    from sys import path as sys_path
    from os import path as P
    sys_path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), ".")))
    from utilities import cprint
    from core.generic import CLI
    from core.applications import main_runner
    from core.volumes import main_volume
    from core.check import main_check


################################################################################
############################# OPTIONS (Decorator) ##############################
################################################################################


def common_options(*args):
    """Decorator to centralize and simplify option management
    ARGS:
        List of options to apply
    RETURN:
        Function decorated with the specified options
    """
    def inner_common_options(func):
        # List options groups
        options = {
            "default": [
                click.help_option('-H', '-h', '--help'),
                click.option(
                    '-S', '--skip-config', 'skip_config', is_flag=True,
                    help='Use the current local configuration without pulling the last changes'),
            ],
            "details": [
                optgroup.group('Details'),
                optgroup.option(
                    '-I', '--infos', is_flag=True,
                    help='Display informations (header)'),
                optgroup.option(
                    '-v', '--verbose', 'verbose', count=True,
                    help="Repetition of the option increases the logs level"),
            ],
            "application": [
                optgroup.group('Application', cls=RequiredMutuallyExclusiveOptionGroup),
                optgroup.option(
                    '-A', '--all', 'application_all', is_flag=True,
                    help='All applications present in the configuration file will be affected'),
                optgroup.option(
                    '-a', '--application', multiple=True,
                    help='Only listed applications which are present in the configuration file will be affected'),
                optgroup.option(
                    '-i', '--ignore-application', multiple=True,
                    help='============ NOT IMPLEMENTED ============ All applications present in the configuration file except listed applications will be affected'),
            ],
            "context": [
                optgroup.group('Context'),
                optgroup.option(
                    '-c', '--context',
                    help='Working context (staging, production...)'),
            ],
            "kubeconfig": [
                optgroup.group('Kubeconfig'),
                optgroup.option(
                    '-k', '--kubeconfig', 'kubeconfig', type=click.Path(exists=False),
                    help='Override the default value with the specified kubeconfig path'),
            ],
            "path": [
                optgroup.group('Path'),
                optgroup.option(
                    '-p', '--path', type=click.Path(exists=False),
                    help='Local work directory (default: `./work`)'),
            ],
            "runner": [
                optgroup.group('Runner', cls=MutuallyExclusiveOptionGroup),
                optgroup.option(
                    '-r', '--runner',
                    help='Override the default value with the specified runner'),
                optgroup.option(
                    '-l', '--list', is_flag=True,
                    help='List of available runners (and their aliases)'),
                optgroup.option(
                    '-e', '--exec',
                    help='Execute a shell command in the app directory'),
            ],
            "execution": [
                optgroup.group('Execution'),
                optgroup.option(
                    '-P', '--parallel', is_flag=True,
                    help='All applications run in parallel. ================= WARNING ================= No interaction is available when this option is used !!!'),
            ],
        }
        # Get and apply decorator options
        args_list = ['default', 'details']          # Force default groups
        args_list.extend(args)                      # Add specified groups
        args_list = OrderedDict.fromkeys(args_list) # Delete duplicated groups
        # For each group...
        for arg in reversed(args_list):
            if arg not in options:
                cprint(f"WARNING: Unknown group {arg}", CLI.verbose)
                continue
            # For each option...
            for option in reversed(options[arg]):
                func = option(func)
        return func
    return inner_common_options


################################################################################
##################################### CLI ######################################
################################################################################


@click.group(invoke_without_command=True)
@click.help_option('-H', '-h', '--help')
@click.pass_context
def cli(ctx):
    if ctx.invoked_subcommand is None:
        command = "tree -I '.git|kmama.egg-info|__pycache__|*.pyc' -a"
        output = s_run(["bash", "-c", command], capture_output=True)
        if not output.returncode:
            cprint(output.stdout.decode())


@cli.command('runner', help='Run specific script for specific application')
@common_options('application', 'context', 'kubeconfig', 'path', 'runner', 'execution')
def runner_command(**kwargs):
    """Run specific script for specific application"""
    CLI.update(kwargs)
    main_runner()


@cli.command('volumes', help='Create default PV/PVC on k8s cluster')
@common_options('context', 'kubeconfig', 'path')
def volumes_command(**kwargs):
    """Create default PV/PVC on k8s cluster"""
    CLI.update(kwargs)
    main_volume()


@cli.command('check', help='Check urls for specific application')
@common_options('application', 'context', 'kubeconfig', 'path', 'execution')
def check_command(**kwargs):
    """Check urls for specific application"""
    CLI.update(kwargs)
    main_check()


if __name__ == "__main__":
    cli()
