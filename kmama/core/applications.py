from multiprocessing import Pool
from os import path as P, makedirs as mkdir_p, chdir
from shutil import rmtree
try:
    from kmama.core.generic import CLI, CONFIG, VERBOSITY, common_settings
    from kmama.core.execution import app_in_parallel, sub_process_run
    from kmama.utilities import get_path, cprint, DotDict, StrFormat
except ImportError:
    from sys import sys_path
    sys_path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from core.generic import CLI, CONFIG, VERBOSITY, common_settings
    from core.execution import app_in_parallel, sub_process_run
    from utilities import get_path, cprint, DotDict, StrFormat


def set_application_name(args, validated, application):
    """Application name"""
    cprint(f"DEBUG: Set {application!r} name", CLI.verbose > VERBOSITY.INFO)
    args.application = application
    return args, validated, application


def set_application_repository(args, validated, application):
    """Application repository"""
    cprint(f"DEBUG: Set {application!r} repository", CLI.verbose > VERBOSITY.INFO)
    # Case 1: repository specified in configuration file
    if CONFIG.applications[application].get("repository"):
        args.repository_name = CONFIG.applications[application].repository
        cprint(f"DEBUG: ... from configuration file ({args.repository_name})", CLI.verbose > VERBOSITY.WARNING)
    # Case 2: default repository is <application>-k8s
    else:
        args.repository_name = f"{application}-k8s"
        cprint(f"DEBUG: ... default value ({args.repository_name})", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, application


def set_application_path(args, validated, application):
    """Application path"""
    cprint(f"DEBUG: Set {application!r} path", CLI.verbose > VERBOSITY.INFO)
    try:
        args.application_path = get_path(P.join(args.work_directory, args.repository_name), check_path=True)
    except FileNotFoundError:
        args.application_path = get_path(P.join(args.work_directory, args.repository_name))
        cprint(f"INFORMATION: {args.application_path} doesn't exist", CLI.verbose)
    cprint(f"DEBUG: ... path: {args.application_path!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, application


def set_application_context_path(args, validated, application):
    """Application context path"""
    cprint(f"DEBUG: Set {application!r} context path", CLI.verbose > VERBOSITY.INFO)
    try:
        args.context_path = get_path(P.join(args.application_path, "deploy", "overlays", args.context), check_path=True)
    except FileNotFoundError:
        args.context_path = get_path(P.join(args.application_path, "deploy", "overlays", args.context))
        cprint(f"INFORMATION: {args.context_path} doesn't exist", CLI.verbose)
    cprint(f"DEBUG: ... path: {args.context_path!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, application


def set_application_runner(args, validated, application):
    """Application runners/scripts"""
    cprint(f"DEBUG: Set {application!r} runners/scripts", CLI.verbose > VERBOSITY.INFO)
    # CLI[runner]: --list (print runners and exit)
    if CLI.list:
        cprint("DEBUG: ... option --list", CLI.verbose > VERBOSITY.WARNING)
        cprint("List of available runners:\n---")
        for runner_name, runner_args in CONFIG.get("runners", {}).items():
            cprint(f"- {runner_name}:\n\t{runner_args.get('description')}")
        if not CONFIG.runners:
            cprint("No runners available")
        cprint("---")
        cprint("INFORMATION: The list of available runners is the same for all applications", CLI.verbose)
        exit(0)
    # CLI[runner]: --exec COMMAND
    if CLI.exec:
        cprint("DEBUG: ... option --exec COMMAND: this is not a runner but a bash command", CLI.verbose > VERBOSITY.WARNING)
        args.runner = f"set -e; cd {args.application_path}; {CLI.exec}; set +e"
        return args, validated, application
    # CLI[runner]: --runner RUNNER
    if CLI.runner:
        cprint("DEBUG: ... option --runner RUNNER: override the default runner", CLI.verbose > VERBOSITY.WARNING)
        runner = CLI.runner
    # no CLI[runner] but runner in CONFIG
    elif CONFIG.applications[application].get("runner"):
        cprint("DEBUG: ... no runner options: use the runner specified in the configuration file", CLI.verbose > VERBOSITY.WARNING)
        runner = CONFIG.applications[application].runner
    # no CLI[runner] and no CONFIG[runner]
    else:
        cprint("DEBUG: ... no runner options, no runners in the configuration file: use the 'default' runner or alias", CLI.verbose > VERBOSITY.WARNING)
        runner = "default"
    # Check if runner is valid
    cprint("DEBUG: ... check if runner is valid", CLI.verbose > VERBOSITY.WARNING)
    for runner_name, runner_args in CONFIG.get("runners", {}).items():
        if runner in runner_args.get("aliases", [runner_name]):
            cprint(f"DEBUG: ... found {runner!r} in the configuration file", CLI.verbose > VERBOSITY.WARNING)
            args.runner = runner_args.get("scripts", [])
            break
    else:
        cprint("ERROR: Invalid runner (not found or missing scripts)")
        validated = False
    return args, validated, application


def set_application_tests(args, validated, application):
    """Application tests (obsolete)"""
    cprint(f"DEBUG: Set {application!r} tests/checks (obsolete)", CLI.verbose > VERBOSITY.INFO)
    if CONFIG.applications[application].get("tests"):
        args.application_tests = CONFIG.applications[application].tests
    else:
        args.application_tests = []
    if isinstance(args.application_tests, str):
        args.application_tests = [args.application_tests]
    elif not isinstance(args.application_tests, list):
        args.application_tests = []
        cprint("WARNING: The test should be string or list", CLI.verbose)
    return args, validated, application


def application_settings(args, application):
    cprint("DEBUG: Start configuring the application settings", CLI.verbose > VERBOSITY.INFO)
    validated = True
    # Application not listed
    if not CONFIG.get("applications") or not application in CONFIG.applications:
        cprint(f"WARNING: Unknown application {application!r}", CLI.verbose)
        return args, False
    # Application listed but empty should be DotDict
    if not CONFIG.applications[application]:
        CONFIG.applications[application] = DotDict()
    # Set attributes
    args, validated, application = set_application_name(args, validated, application)
    args, validated, application = set_application_repository(args, validated, application)
    args, validated, application = set_application_path(args, validated, application)
    args, validated, application = set_application_context_path(args, validated, application)
    args, validated, application = set_application_runner(args, validated, application)
    args, validated, application = set_application_tests(args, validated, application)
    # Return application settings previously set
    return args, validated


def app_execution(args, application):
    """For each application, execute the specified command/runner
    args: common arguments as path, context, kubeconfig
    application: application name
    """
    chdir(args.current_path)
    cprint(f"\n========== {application} ==========\n")
    cprint(f"DEBUG: Set {application!r} configuration", CLI.verbose > VERBOSITY.INFO)
    args, validated = application_settings(args, application)
    if validated:
        cprint(f"\n>>> path: {args.application_path!r}\n")
        # Case 1: simple shell command
        if CLI.exec:
            cprint(f"\n>>> script: {args.runner!r}:\n")
            cprint("DEBUG: Option --exec COMMAND", CLI.verbose > VERBOSITY.INFO)
            sub_process_run(args.runner, args.context)
            return
        # Case 2: runner from config file
        cprint(f"DEBUG: Runner contains: {args.runner}", CLI.verbose > VERBOSITY.INFO)
        for index, script in enumerate(args.runner):
            if not script:
                cprint("DEBUG: ... empty script ignored", CLI.verbose > VERBOSITY.WARNING)
                continue
            cprint(f"\n>>> script: {script!r}:\n")
            if not CONFIG.get("scripts") or not CONFIG.scripts.get(script):
                cprint(f"ERROR: {script!r} not found in config file. Cancel")
                return
            instructions = [instruction.format_map(StrFormat(**args)) for instruction in CONFIG.scripts[script]]
            if sub_process_run("\n".join(instructions), args.context, (index+1)!=len(args.runner)):
                return
    # Case 3: Invalid configuration: Nothing


def main_runner():
    # Builds the "args" dictionary containing the information common to the whole program
    args, validated = common_settings()
    if not validated:
        cprint("ERROR: Default configuration invalid")
        return
    # Call exec function
    if CLI.parallel:
        # Multi processing
        mkdir_p(args.processes_directory, exist_ok=True)
        arguments = [[args.to_dict(), application, app_execution] for application in (args.applications)]
        with Pool(5) as pool:
            pool.map(app_in_parallel, arguments)
        rmtree(args.processes_directory)
    else:
        # One process
        for application in args.applications:
            app_execution(args, application)
