from multiprocessing import Pool
from os import path as P, makedirs as mkdir_p
from shutil import rmtree
from subprocess import run as s_run
try:
    from kmama.core.generic import VERBOSITY, CLI, CONFIG, DRYRUN, common_settings
    from kmama.core.execution import tests_execution, app_in_parallel
    from kmama.utilities import cprint
except ImportError:
    from sys import path as sys_path
    sys_path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from core.generic import VERBOSITY, CLI, CONFIG, DRYRUN, common_settings
    from core.execution import tests_execution, app_in_parallel
    from utilities import cprint


def format_application_tests(tests, application):
    # tests: my_url
    if isinstance(tests, str):
        return [{"request": tests, "return_code": 200}]
    # tests: [my_url]
    if isinstance(tests, list):
        app_tests = []
        for test in tests:
            formatted_test = format_application_tests(test, application)
            if formatted_test is None:
                continue
            app_tests.extend(formatted_test)
        return app_tests
    # tests: {key: my_url}
    if isinstance(tests, dict):
        app_tests = {}
        for key, value in tests.items():
            if key == "request" and isinstance(value, str):
                app_tests[key] = value
            elif key == "request":
                cprint(f"WARNING: {application!r} - Invalid URL to test ({value})", CLI.verbose)
                return
            if key == "return_code" and isinstance(value, int):
                app_tests[key] = value
            elif key == "return_code":
                cprint(f"WARNING: {application!r} - Return code should be an integer", CLI.verbose)
                return
        if "request" not in app_tests:
            cprint(f"WARNING: {application!r} - No URL to test or invalid key (expected: 'request')", CLI.verbose)
            return
        if "return_code" not in app_tests:
            app_tests["return_code"] = 200
        return [app_tests]
    # tests: 123
    cprint(f"WARNING: {application!r} - URL {str(tests)!r} cannot be of type {type(tests)!r}", CLI.verbose)
    return


def application_exists(application):
    cprint(f"DEBUG: Check if {application!r} exists in the configuration file", CLI.verbose > VERBOSITY.INFO)
    return CONFIG.get("applications") and application in CONFIG.applications


def application_contains_tests(application):
    cprint(f"DEBUG: Check if {application!r} contains tests", CLI.verbose > VERBOSITY.INFO)
    return CONFIG.applications[application] and CONFIG.applications[application].get("tests")


def application_service_is_running(application, test__command):
    cprint(f"DEBUG: Check if {application!r} is running (`kubectl get service` returncode should be 0)", CLI.verbose > VERBOSITY.INFO)
    return not s_run(["bash", "-c", test__command], capture_output=True).returncode


def app_check(args, application):
    cprint(f"\n=== {application!r} ===")
    # Build kubectl commands
    cprint(f"DEBUG: Set {application!r} configuration", CLI.verbose > VERBOSITY.INFO)
    kubectl = f"kubectl --kubeconfig {args.kubeconfig} --namespace {args.context}"
    resource = f"service/{args.context}-{application}-service"
    kubectl_get = f"{kubectl} get {resource}"
    kubectl_forward = f"{kubectl} port-forward {resource}"
    # Check if application exists in CONFIG dict
    if not application_exists(application):
        cprint(f"WARNING: Unknown application {application!r}", CLI.verbose)
        cprint("===")
        return
    # Check if application contains tests
    if not application_contains_tests(application):
        cprint(f"INFORMATION: No tests for the application {application!r}", CLI.verbose)
        cprint("===")
        return
    # Check if tests are valid
    cprint(f"DEBUG: Check if {application!r} tests are valid", CLI.verbose > VERBOSITY.INFO)
    tests = format_application_tests(CONFIG.applications[application].tests, application)
    if not tests:
        return
    if args.context == DRYRUN:
        cprint(f"DEBUG: {DRYRUN!r} context has no effect. Try another one", CLI.verbose)
        cprint("===")
        return
    # Check if application contains service on the cluster
    if not application_service_is_running(application, kubectl_get):
        print(kubectl_get)
        cprint(f"WARNING: No services found for the application {application!r}", CLI.verbose)
        cprint("===")
        return
    # All is good, run tests
    cprint(f"DEBUG: Start {application!r} tests", CLI.verbose > VERBOSITY.INFO)
    tests_execution(tests, kubectl_get, kubectl_forward)
    cprint("===")


def main_check():
    # Builds the "args" dictionary containing the information common to the whole program
    args, validated = common_settings()
    if not validated:
        cprint("ERROR: Default configuration invalid")
        return
    # Call exec function
    if CLI.parallel:
        # Multi processing
        mkdir_p(args.processes_directory, exist_ok=True)
        arguments = [[args.to_dict(), application, app_check] for application in (args.applications)]
        with Pool(5) as pool:
            pool.map(app_in_parallel, arguments)
        rmtree(args.processes_directory)
    else:
        # One process
        for application in args.applications:
            app_check(args, application)
