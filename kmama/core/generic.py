from collections import OrderedDict
from os import path as P, listdir, makedirs as mkdir_p
from subprocess import run as s_run, SubprocessError
from shutil import rmtree
from yaml import safe_load as yaml_load
from termcolor import colored
try:
    from kmama.utilities import (
        DotDict, DotStruct,
        get_path, pull_or_clone, cprint
    )
except ImportError:
    from sys import path as sys_path
    sys_path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from utilities import (
        DotDict, DotStruct,
        get_path, pull_or_clone, cprint
    )

DRYRUN = "dryrun"
CLI = DotDict()
CONFIG = DotDict()
VERBOSITY = DotDict({
    'NULL': 0,
    'INFO': 1,
    'WARNING': 2,
    'ERROR': 3,
})


################################################################################
############################# Configuration files ##############################
################################################################################


def check_config_error(config_dict):
    """['constants', 'volumes', 'contexts', 'scripts', 'runners', 'applications']"""
    cprint("DEBUG: Check if configuration file is valid", CLI.verbose > VERBOSITY.INFO)
    for main_key, main_value in config_dict.items():
        if main_key == 'constants':
            required_type = str
        elif main_key == 'contexts':
            required_type = dict
        elif main_key == 'volumes':
            required_type = dict, list, str
        # elif main_key == 'functions':
        #     required_type = list
        elif main_key == 'scripts':
            required_type = list
        elif main_key == 'runners':
            required_type = dict
        elif main_key == 'applications':
            required_type = dict
        else:
            cprint(f"WARNING: Unknown key {main_key!r}", CLI.verbose)
            continue
        for key, value in main_value.items():
            if not isinstance(value, (required_type, type(None))):
                cprint(f"ERROR: '{main_key}[{key}]' type should be {required_type!r} and not {type(value)!r}")
                return True
    return False


def load_file(f_path:str, f_name:str, is_patch:bool=False):
    """Load a config file and update CONFIG dict
    Args:
        f_path (str): file path
        f_name (str): file name
        is_patch (bool, optional (Defaults to False)): flag indicate if file is default or patch.
    Returns:
        list: list all configuration files used with the current file if required
    """
    f_path = P.join(f_path, f_name)
    # if it's a yaml file
    if P.isfile(f_path) and f_name.endswith((".yaml", ".yml")):
        cprint(f"DEBUG: ... - loading {f_name!r}", CLI.verbose > VERBOSITY.WARNING)
        with open(f_path, 'r', encoding='utf-8') as f:
            # ...load the file (should contain a dict)...
            try:
                f_config = DotStruct(yaml_load(f))
            except Exception:
                cprint(f"ERROR: Unable to load configuration file: {f_path!r}")
                exit(1)
            if not isinstance(f_config, DotDict):
                cprint(f"WARNING: {f_path!r} is not a valid configuration file.", CLI.verbose)
                return
            if is_patch:
                # ...for each nested key/value, update CONFIG...
                CONFIG.merge(f_config)
            else:
                # ...update CONFIG
                CONFIG.update(f_config)


def load_configuration(args, validated, infos):
    """Select and load config file"""
    cprint("DEBUG: Load configuration file", CLI.verbose > VERBOSITY.INFO)
    # Pull or clone configuration if required
    if not CLI.skip_config:
        cprint("DEBUG: ... pull or clone 'kmama-config' repository", CLI.verbose > VERBOSITY.WARNING)
        rmtree(args.default_directory, ignore_errors=True)
        mkdir_p(args.default_directory)
        if not pull_or_clone('git@gricad-gitlab.univ-grenoble-alpes.fr:OSUG/RESIF/', 'kmama-config', args.default_directory):
            cprint("ERROR: Unable to get 'kmama-config' repository")
            exit(1)
    # Select config from CLI > Default repository
    cprint(f"DEBUG: ... search default files in {args.default_directory!r} path", CLI.verbose > VERBOSITY.WARNING)
    for filename in listdir(args.default_directory):
        load_file(args.default_directory, filename)
    # Override contexts
    cprint(f"DEBUG: ... search overload files in {args.configuration_path!r} path", CLI.verbose > VERBOSITY.WARNING)
    for filename in listdir(args.configuration_path):
        load_file(args.configuration_path, filename, is_patch=True)
    if check_config_error(CONFIG):
        cprint(f"ERROR: {args.configuration_path!r} must contain configuration files whose values are incorrect and overload the default values")
        exit(1)
    return args, validated, infos


################################################################################
#################################### PATHS #####################################
################################################################################


def set_configuration_directory(args, validated, infos):
    """Set configuration path"""
    cprint("DEBUG: Set configuration path", CLI.verbose > VERBOSITY.INFO)
    args.configuration_path = get_path("~/.config/kmama")
    infos["Configuration path"] = args.configuration_path
    cprint(f"DEBUG: ... configuration path: {args.configuration_path!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_current_directory(args, validated, infos):
    """Set current path"""
    cprint("DEBUG: Set current directory", CLI.verbose > VERBOSITY.INFO)
    args.current_path = get_path(from_script_path=False)
    infos["Current path"] = args.current_path
    cprint(f"DEBUG: ... path: {args.current_path!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_sources_directory(args, validated, infos):
    """Set sources directory"""
    cprint("DEBUG: Set __script__ directory", CLI.verbose > VERBOSITY.INFO)
    args.sources_directory = get_path(from_script_path=True)
    infos["Sources directory"] = args.sources_directory
    cprint(f"DEBUG: ... path: {args.sources_directory!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_work_directory(args, validated, infos):
    """Set work directory"""
    cprint("DEBUG: Set work directory", CLI.verbose > VERBOSITY.INFO)
    # Choose path from CLI > CONFIG > Default
    if CLI.path:
        args.work_directory = CLI.path
        cprint("DEBUG: ... from command line", CLI.verbose > VERBOSITY.WARNING)
    elif CONFIG.get("constants") and CONFIG.constants.get("work_directory"):
        args.work_directory = CONFIG.constants.work_directory
        cprint("DEBUG: ... from configuration file", CLI.verbose > VERBOSITY.WARNING)
    else:
        args.work_directory = "./work"
        cprint("DEBUG: ... from default (./work)", CLI.verbose > VERBOSITY.WARNING)
    args.work_directory = get_path(args.work_directory, from_script_path=False)
    mkdir_p(args.work_directory, exist_ok=True)
    infos["Work directory"] = args.work_directory
    cprint(f"DEBUG: ... path: {args.work_directory!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_default_directory(args, validated, infos):
    """Set directory for default configuration"""
    cprint("DEBUG: Set directory for default configuration", CLI.verbose > VERBOSITY.INFO)
    args.default_directory = P.join(args.configuration_path, ".default")
    mkdir_p(args.default_directory, exist_ok=True)
    cprint(f"DEBUG: ... path: {args.default_directory!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_volumes_directory(args, validated, infos):
    """Set volumes directory for generated pv/pvc"""
    cprint("DEBUG: Set volumes directory for generated pv/pvc", CLI.verbose > VERBOSITY.INFO)
    args.volumes_directory = P.join(args.configuration_path, ".volumes")
    mkdir_p(args.volumes_directory, exist_ok=True)
    cprint(f"DEBUG: ... path: {args.volumes_directory!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_processes_directory(args, validated, infos):
    """Set output process directory for parallel mode"""
    cprint("DEBUG: Set directory for subprocesses output", CLI.verbose > VERBOSITY.INFO)
    args.processes_directory = P.join(args.configuration_path, ".processes.out")
    mkdir_p(args.processes_directory, exist_ok=True)
    cprint(f"DEBUG: ... path: {args.processes_directory!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


################################################################################
############################# Other configurations #############################
################################################################################


def build_namespace(kubeconfig, context):
    cprint(f"DEBUG: Check if {context!r} namespace exists", CLI.verbose > VERBOSITY.INFO)
    try:
        s_run(f"kubectl --kubeconfig {kubeconfig} get namespace {context}".split(), check=True, capture_output=True)
        cprint("DEBUG: ... OK: namespace found", CLI.verbose > VERBOSITY.WARNING)
    except SubprocessError:
        cprint(f"INFORMATION: The {context} namespace doesn't exist yet, it will be created...", CLI.verbose)
        s_run(f"kubectl --kubeconfig {kubeconfig} create namespace {context}".split(), check=True, capture_output=True)


def set_default_constants(args, validated, infos):
    """Constants from CONFIG"""
    cprint("DEBUG: Set constants from configuration file", CLI.verbose > VERBOSITY.INFO)
    if CONFIG.get("constants"):
        for constant in CONFIG.constants:
            args[constant] = CONFIG.constants[constant]
            cprint(f"DEBUG: ... - {constant!r}= {args[constant]!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_context(args, validated, infos):
    """Set context"""
    cprint("DEBUG: Set context", CLI.verbose > VERBOSITY.INFO)
    # Choose context from CLI > Default (dryrun)
    if CLI.context:
        args.context = CLI.context
        cprint("DEBUG: ... from command line", CLI.verbose > VERBOSITY.WARNING)
    else:
        args.context = DRYRUN
        cprint(f"DEBUG: ... from default ({DRYRUN})", CLI.verbose > VERBOSITY.WARNING)
    # Context should be in configuration file
    args.context_message = ""
    if not CONFIG.get("contexts") or not CONFIG.contexts.get(args.context):
        cprint("WARNING: This context is not present in the configuration file")
        validated = False
    # Set context message if present
    elif CONFIG.contexts[args.context].get('message'):
        args.context_message = CONFIG.contexts[args.context].message
    infos["Current context"] = args.context
    infos["Contexts list"] = list(CONFIG.contexts) if CONFIG.get("contexts") else []
    cprint(f"DEBUG: ... context: {args.context!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_kubeconfig(args, validated, infos):
    """Set Kubeconfig"""
    cprint("DEBUG: Set kubeconfig", CLI.verbose > VERBOSITY.INFO)
    # Select kubeconfig from CLI > CONFIG.context > Default
    if CLI.kubeconfig:
        args.kubeconfig = CLI.kubeconfig
        cprint("WARNING: You specify a kubeconfig manually, the message indicating the cluster may be wrong.")
        cprint("DEBUG: ... from command line", CLI.verbose > VERBOSITY.WARNING)
    elif CONFIG.get("contexts") and CONFIG.contexts.get(args.context) and CONFIG.contexts[args.context].get("kubeconfig"):
        args.kubeconfig = CONFIG.contexts[args.context].kubeconfig
        cprint("DEBUG: ... from configuration file", CLI.verbose > VERBOSITY.WARNING)
    else:
        args.kubeconfig = "~/.kube/config"
        cprint("DEBUG: ... from default (~/.kube/config)", CLI.verbose > VERBOSITY.WARNING)
        args.context_message = "WARNING: You are working with the default Kubeconfig, check that you are on the right cluster."
    # Check if file exist
    cprint("DEBUG: ... check if kubeconfig file exist", CLI.verbose > VERBOSITY.WARNING)
    try:
        args.kubeconfig = get_path(args.kubeconfig, check_path=True, from_script_path=False)
    except FileNotFoundError:
        cprint(f"ERROR: {args.kubeconfig} doesn't exist")
        validated = False
    infos["Current kubeconfig"] = args.kubeconfig
    infos["Kubeconfig list"] = [filename for filename in listdir(get_path("~/.kube/")) if P.isfile(get_path(f"~/.kube/{filename}"))]
    cprint(f"DEBUG: ... kubeconfig: {args.kubeconfig!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def set_base_url(args, validated, infos):
    """Set base_url (git)"""
    cprint("DEBUG: Set git base url", CLI.verbose > VERBOSITY.INFO)
    # Select base_url from CONFIG > Default
    if CONFIG.get("constants") and CONFIG.constants.get("base_url"):
        args.base_url = CONFIG.constants.base_url
    else:
        args.base_url = "git@gricad-gitlab.univ-grenoble-alpes.fr:OSUG/RESIF"
    infos["Git base URL"] = args.base_url
    cprint(f"DEBUG: ... url: {args.base_url!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


def list_applications(args, validated, infos):
    """List applications"""
    cprint("DEBUG: Set applications list", CLI.verbose > VERBOSITY.INFO)
    # The CLI does not require an application list
    if not CLI.get("application_all") and not CLI.get("application"):
        args.applications = None
        cprint("DEBUG: ... the current command does not require applications", CLI.verbose > VERBOSITY.WARNING)
    # CLI: --all + CONFIG.applications
    elif CLI.application_all and CONFIG.get("applications"):
        args.applications = list(CONFIG.applications.keys())
        cprint(f"DEBUG: ... all applications listed in the config file {args.applications}", CLI.verbose > VERBOSITY.WARNING)
    # CLI: -a ...
    elif CLI.application:
        args.applications = list(CLI.application)
        cprint(f"DEBUG: ... all applications listed on the command line {args.applications}", CLI.verbose > VERBOSITY.WARNING)
    # Other cases
    else:
        args.applications = []
        cprint("WARNING: No applications in the configuration file")
    infos["Applications list"] = args.applications
    return args, validated, infos


def set_verbosity_level(args, validated, infos):
    """Set verbosity level"""
    cprint("DEBUG: Set verbosity level", CLI.verbose > VERBOSITY.INFO)
    args.verbosity_level = int(CLI.verbose)
    cprint(f"DEBUG: ... level: {args.verbosity_level!r}", CLI.verbose > VERBOSITY.WARNING)
    return args, validated, infos


# from yaml import safe_load as yaml_load
# def get_server_ip(kubeconfig_path):
#     with open(kubeconfig_path, 'r', encoding='utf-8') as kubeconfig:
#         server = yaml_load(kubeconfig)["clusters"][0]["cluster"]["server"]
#         server_ip = find_between(server, "https://", ":6443")
#         print(server_ip)
#         return server_ip



################################################################################
############################# Summary and execution ############################
################################################################################


def display_informations(infos, validated):
    """Display general information and request confirmation before continuing (CLI: --infos)"""
    # KMAMA
    cprint("\n-- KUBERNETES MANIFEST MANAGER --")
    git_path = P.join(infos['Sources directory'], "..")
    git_commands = f"""
    cd {git_path}
    echo "Current branch: $(git branch --show-current)"
    git fetch --all  > /dev/null 2>&1
    UPSTREAM='@{{u}}'
    LOCAL=$(git rev-parse @ || echo "WARNING: Unable to find the local commit")
    REMOTE=$(git rev-parse "$UPSTREAM" 2>/dev/null || echo "WARNING: Unable to find the remote commit")
    BASE=$(git merge-base @ "$UPSTREAM" 2>/dev/null || echo "WARNING: Unable to find the base commit")
    echo -e "- Local commit: $LOCAL \n- Remote commit: $REMOTE \n- Base commit: $BASE"
    if [[ $LOCAL = $REMOTE ]]; then
        echo "INFORMATION: Up to date: nothing to do "
    elif [[ $LOCAL = $BASE ]]; then
        echo "INFORMATION: You have 1 or more late commits: you should pull"
        echo "Files to be updated:"
        git diff --name-only $LOCAL $REMOTE
    elif [[ $REMOTE = $BASE ]]; then
        echo "INFORMATION: You have 1 or more commits in advance: you should push"
        echo "Untracked files:"
        git diff --name-only $LOCAL $REMOTE
    else
        echo "WARNING: Your local branch and the remote branch have diverged. "
    fi
    if [[ -n $(git status --short) ]]; then
        echo
        echo "INFORMATION: Your local repository contains unsaved changes: you should add/remove them"
        echo "Untracked files:"
        git status --short
    fi
    """
    try:
        s_run(["bash", "-c", git_commands], check=True)
    except SubprocessError:
        cprint("WARNING: Impossible to check the status of the kmama repository")
    finally:
        cprint("--\n")
    # Display general information
    if validated:
        cprint(colored("-- VALID CONFIGURATION --", "green"))
    else:
        cprint(colored("-- INVALID CONFIGURATION --", "red"))
    for key, value in infos.items():
        cprint(f"{key:20}:\t{value!r}")
    cprint("--\n")
    # Request confirmation before continuing
    choice = input("Here is the current configuration. Do you want continue ? [Y/N]\n>>> ").lower()
    if choice in ("o", "y", "oui", "yes"):
        cprint("OK. Please wait a few seconds...\n")
    else:
        cprint("Cancel.")
        exit(1)


def common_settings():
    """Build 2 common settings dictionnaries
    - 'args' dict used for the rest of the program
    - 'infos' ordered dictionary used only to display information at the beginning of the program
    Return the first dict and a boolean indicating if the configuration is valid
    """
    # Init dictionnaries
    cprint("DEBUG: Start configuring the common settings", CLI.verbose > VERBOSITY.INFO)
    validated = True
    args = DotDict()
    infos = OrderedDict({
        "Current path": None,
        "Sources directory": None,
        "Work directory": None,
        "Configuration path": None,
        "Current context": None,
        "Contexts list": None,
        "Current kubeconfig": None,
        "Kubeconfig list": None,
        "Git base URL": None,
        "Applications list": None,
    })
    # Fill dictionnaries
    # (Be careful about the order in which the functions are called)
    args, validated, infos = set_current_directory(args, validated, infos)
    args, validated, infos = set_configuration_directory(args, validated, infos)
    args, validated, infos = set_sources_directory(args, validated, infos)
    args, validated, infos = set_processes_directory(args, validated, infos)
    args, validated, infos = set_volumes_directory(args, validated, infos)
    args, validated, infos = set_default_directory(args, validated, infos)
    args, validated, infos = load_configuration(args, validated, infos)
    args, validated, infos = set_default_constants(args, validated, infos)
    args, validated, infos = set_work_directory(args, validated, infos)
    args, validated, infos = set_context(args, validated, infos)
    args, validated, infos = set_kubeconfig(args, validated, infos)
    args, validated, infos = set_base_url(args, validated, infos)
    args, validated, infos = set_verbosity_level(args, validated, infos)
    args, validated, infos = list_applications(args, validated, infos)
    # Context message:
    if args.context_message:
        lenght = len(args.context_message)
        cprint(colored(f"{lenght*'='}\n{args.context_message}\n{lenght*'='}", 'magenta'))
    # Display current configuration for validation
    if CLI.infos:
        display_informations(infos, validated)
    # Build namespace if required
    if validated and args.context != DRYRUN:
        build_namespace(args.kubeconfig, args.context)
    # Return common settings previously set
    return args, validated
