from os import path as P, getpid
from subprocess import run as s_run, Popen, DEVNULL
import sys
from socket import socket, AF_INET, SOCK_STREAM
from time import sleep, time
from requests import get as wget
from termcolor import colored
try:
    from kmama.core.generic import CLI, DRYRUN, VERBOSITY
    from kmama.utilities import DotDict, cprint
except ImportError:
    from sys import path as sys_path
    sys_path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from core.generic import CLI, DRYRUN, VERBOSITY
    from utilities import DotDict, cprint


def sub_process_run(command, context, hidden=False):
    """If not dryrun context:
    - Run the command passed in parameter in a subprocess
    - Display its output
    - Return the exit code
    """
    if context == DRYRUN or CLI.verbose > VERBOSITY.ERROR:
        cprint(colored(f"{command}", 'cyan'))
        if context == DRYRUN:
            return 0
    cprint("DEBUG: Start a subprocess", CLI.verbose > VERBOSITY.INFO)
    if CLI.get('parallel'): # No interaction possible. Colored output.
        output = s_run(["bash", "-c", command], capture_output=True)
        cprint("DEBUG: Subprocess should be closed", CLI.verbose > VERBOSITY.INFO)
        if output.returncode and output.stderr.decode():
            cprint(colored(f"\n-- AN ERROR OCCURRED --\n{output.stderr.decode()}\n--", 'red'))
        elif output.returncode and output.stdout.decode():
            cprint(colored(f"\n-- AN ERROR OCCURRED --\n{output.stdout.decode()}\n--", 'red'))
        elif not hidden or CLI.verbose:
            cprint(colored(output.stdout.decode() or "\n-- SUCCESS --\n", 'green'))
    else: # Interaction with stdin. No colors.
        output = s_run(["bash", "-c", command])
        cprint("DEBUG: Subprocess should be closed", CLI.verbose > VERBOSITY.INFO)
        if output.returncode:
            cprint(colored("\n-- AN ERROR OCCURRED --\n", 'red'))
        elif not hidden or CLI.verbose:
            cprint(colored("\n-- SUCCESS --\n", 'green'))
    return output.returncode


def app_in_parallel(argument):
    """Run application and capture output by process"""
    # Unpack arguments
    func = argument[2]
    application = argument[1]
    args = DotDict(argument[0])
    # Temporary file
    tmp_file = P.join(args.processes_directory, f"{getpid()}.out")
    # Save and redirect output
    start_time = time()
    cprint(f"DEBUG: Starts capturing the output for {application}", CLI.verbose > VERBOSITY.WARNING)
    old_stdout = sys.stdout
    sys.stdout = open(tmp_file, "w", encoding='utf-8')
    # Execution
    func(args, application)
    # Restore output
    sys.stdout = old_stdout
    end_time = time()
    cprint(f"DEBUG: Restore standard output for {application} ({end_time-start_time} sec)", CLI.verbose > VERBOSITY.WARNING)
    cprint(open(tmp_file, "r", encoding='utf-8').read())


def tests_execution(tests, kubectl_get, kubectl_forward):
    # Build arguments
    service_port = f"$(echo $({kubectl_get} -o custom-columns=:.spec.ports[0].port | sed 's/ //g' ))"
    cprint("DEBUG: Search free and open port", CLI.verbose > VERBOSITY.INFO)
    with socket(AF_INET, SOCK_STREAM) as sock:
        sock.bind(('', 0))
        local_port = sock.getsockname()[1]
    domain = f"http://localhost:{local_port}"
    # Start daemon
    cprint(f"Start forwarding ({domain})...")
    daemon = Popen(["bash", "-c", f"{kubectl_forward} {local_port}:{service_port}"], stdout=DEVNULL)
    sleep(4) # Wait until the daemon is running
    # Launch tests
    for test in tests:
        # Format URL
        cprint("DEBUG: ... build URL", CLI.verbose > VERBOSITY.WARNING)
        request_url = test['request']
        return_code = test['return_code']
        link = domain + (request_url if request_url.startswith('/') else f"/{request_url}")
        cprint(link)
        # Execute request
        cprint("DEBUG: ... start request", CLI.verbose > VERBOSITY.WARNING)
        try:
            req = wget(link, headers={'User-Agent': 'Résif-DC KMAMA (`kmama check`)'})
        except Exception as error:
            cprint(f"DEBUG: {error}", CLI.verbose > VERBOSITY.INFO)
            cprint("ERROR: Unable to access service")
        # Check status
        else:
            if req.status_code == return_code:
                cprint(colored(f"SUCCESS: code {req.status_code}", "green"))
            else:
                cprint(colored(f"ERROR: code {req.status_code} instead of {return_code}", "red"))
    # Kill daemon
    daemon.kill()
