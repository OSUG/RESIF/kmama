from os import path as P, getcwd
from yaml import dump as yaml_dump, Dumper
from termcolor import cprint as color_print
from git import Repo, GitCommandError, InvalidGitRepositoryError, NoSuchPathError

# from datetime import datetime as dt
# dt.now().strftime("[%m/%d/%Y %H:%M:%S]")

__author__ = "Thibault ROLLAND"


def get_path(new_path=None, check_path=False, from_script_path=True) -> str:
    """
    This function is used to get and format a path.
    TODO: isfile, isdir, "spaces in path"...
    """
    path = P.split(P.abspath(__file__))[0] \
        if from_script_path \
        else getcwd()
    if isinstance(new_path, str):
        new_path = new_path.strip()
        if new_path.startswith("/"):
            path = P.normpath(new_path)
        elif new_path.startswith("~"):
            path = P.expanduser(new_path)
        else:
            path = P.join(path, new_path)
    path = str(P.normpath(path))
    if check_path and not P.exists(path):
        raise FileNotFoundError(f"'{path}' does not exist.")
    return path


def pull_or_clone(base_url: str, repository_name: str, local_path: str=None):
    """Try to clone or pull a repository.
    Return boolean True if success, False else"""
    local_path = get_path(local_path or '.')
    if base_url.endswith('/') and repository_name.startswith('/'):
        repository_url = base_url + repository_name[1:]
    elif base_url.endswith('/') or repository_name.startswith('/'):
        repository_url = base_url + repository_name
    else:
        repository_url = base_url + "/" + repository_name
    try:
        Repo.clone_from(repository_url, local_path)
    except GitCommandError:
        try:
            Repo(local_path).remotes.origin.pull()
        except (NoSuchPathError, InvalidGitRepositoryError):
            cprint(f"Unable to get the '{repository_name}' repository, maybe you have a local repository not up to date.")
            return False
    return True
            

def conditional_print(string: str, condition: bool=True):
    """Display the first argument if the condition given in the second argument is true"""
    if condition:
        if string.startswith("SUCCESS:"):
            color_print(string, "green")
        elif string.startswith("INFORMATION:"):
            color_print(string, "cyan")
        elif string.startswith("WARNING:"):
            color_print(string, "yellow")
        elif string.startswith("ERROR:"):
            color_print(string, "red")
        else:
            print(string)
    return condition


def find_between(string: str, before_string: str, after_string: str):
    """Extract a substring between two substrings"""
    # return string[len(before_string):-len(after_string)]
    return string.split(before_string)[1].split(after_string)[0]


def pretty_print(item: object, repr: bool=False) -> str:
    """
    Display the content of the object in yaml format.
    """
    Dumper.ignore_aliases = lambda *args : True
    try:
        output = yaml_dump(
            item,
            indent=4,
            sort_keys=False,
            default_flow_style=False,
        )
    except KeyError as err:
        output = (
            f"ERROR: {err.__class__!r}({err.args})\n"
            f"\tFile: {err.__traceback__.tb_frame.f_code.co_filename!r}\n"
            f"\tFunc: {err.__traceback__.tb_frame.f_code.co_name!r}\n"
            f"\tItem: {type(item)!r}"
        )
    real_output = ""
    for line in output.splitlines():
        real_output += f"{line.split('!!python/object:')[0]}\n"
    if repr:
        return real_output or output
    print(real_output or output)


def print_debug(item: object, verbose: bool=False) -> None:
    """
    Display indented objects with type with more or less verbosity
    """
    def _print(item, spaces, verbose):
        if verbose:
            print(f"{spaces}{item}\n{spaces}{type(item)}")
        else:
            print(f"{spaces}{str(item)[:10]}... : {type(item)}")

    def _browser(item, i_level):
        i_size = "  "
        _print(item, i_level*i_size, verbose)
        if isinstance(item, dict):
            for key, value in item.items():
                _print(key, (i_level+1)*i_size, verbose)
                _browser(value, i_level+2)
        elif isinstance(item, list):
            for element in item:
                _browser(element, i_level+1)

    _browser(item, i_level=0)


def map_item(func, item):
    """
    As default `map`: apply function on all elements in item
    """
    result = type(item)()
    if isinstance(item, dict):
        for key, value in item.items():
            result[key] = func(value)
    elif isinstance(item, list):
        for element in item:
            result.append(func(element))
    else:
        result = item
    return result


def to_dot_format(item):
    """
    Convert object (list or dict) to DotStruct (DotList or DotDict).
    Ignore other types (str, int...)    
    """
    if isinstance(item, dict):
        return DotDict(map_item(to_dot_format, (item)))
    if isinstance(item, list):
        return DotList(map_item(to_dot_format, (item)))
    return item


def from_dot_format(item):
    """
    Convert DotStruct (DotList or DotDict) to object (list or dict)
    Ignore other types (str, int...)    
    """
    if isinstance(item, dict):
        return (map_item(from_dot_format, dict(item)))
    if isinstance(item, list):
        return (map_item(from_dot_format, list(item)))
    return item


def nested_list_to_dict(item: dict) -> dict:
    """
    Transform a dictionary containing lists into nested dictionaries.
    """
    def list_to_dict(item: list) -> dict:
        return dict(enumerate(item))

    new_item = {}
    for key, value in item.items():
        if isinstance(value, dict):
            new_item[key] = nested_list_to_dict(value)
        elif isinstance(value, list):
            new_item[key] = nested_list_to_dict(list_to_dict(value))
        else:
            new_item[key] = value
    return new_item


def nested_dict_to_list(item: dict) -> dict:
    """
    Re-transforms nested dictionaries into list dictionaries.
    """
    def dict_to_list(item: dict) -> list:
        return list(item.values())

    new_item = {}
    for key, value in item.items():
        if isinstance(value, dict):
            sub_keys = list(value.keys())
            if (
                (all([isinstance(key, int) for key in sub_keys])) and
                (sorted(sub_keys) == list(range(len(sub_keys))))
            ):
                new_item[key] = dict_to_list(nested_dict_to_list(value))
            else:
                new_item[key] = nested_dict_to_list(value)
        else:
            new_item[key] = value
    return new_item


def nested_merge(merged_dict, merging_dict):
    """
    merging_dict: source
    merged_dict: dest
    """
    for merging_key, merging_value in merging_dict.items():
        if isinstance(merging_value, dict):
            nested_merge(merged_dict.setdefault(merging_key, {}), merging_value)
        elif merging_value:
            merged_dict[merging_key] = merging_value
    return merged_dict


class StringFormat(dict):
    """Dictionary that returns key=key if the requested key does not exist"""
    def __missing__(self, key):
        """Ex:
        >>> string_format = StringFormat()
        >>> "Hello, my name is {name}".format(**string_format)
        Hello, my name is name
        """
        return f"{{{key}}}"


class DotDict(dict):
    def __init__(self, *args, **kwargs):
        super().__init__()
        for key, value in dict(*args, **kwargs).items():
            self.__setitem__(key, value)

    def setdefault(self, key, default=None):
        if key not in self:
            self.__setitem__(key, default)
        return self[key]

    def __or__(self, item):
        if not isinstance(item, dict):
            return NotImplemented
        new_item = DotDict(self)
        new_item.update(item)
        return new_item

    def __ror__(self, item):
        if not isinstance(item, dict):
            return NotImplemented
        return DotDict.__or__(item, self)

    def __ior__(self, item):
        self.update(item)
        return self

    def __setitem__(self, key, value):
        super().__setitem__(key, to_dot_format(value))

    __getattr__ = dict.__getitem__
    __delattr__ = dict.__delitem__
    __setattr__ = __setitem__
    update = __init__
    to_dict = from_dot_format
    nested_dict = nested_list_to_dict
    nested_list = nested_dict_to_list
    merge = nested_merge


class DotList(list):
    def __init__(self, *args, **kwargs):
        super().__init__()
        for index, element in enumerate(list(*args, **kwargs)):
            self.insert(index, element)

    def append(self, item):
        self.insert(len(self), item)

    def extend(self, item):
        for element in item:
            self.insert(len(self), element)
        return self

    def insert(self, index, value):
        super().insert(index, to_dot_format(value))

    def __add__(self, other):
        return to_dot_format(list(self) + list(other))
    to_list = from_dot_format


StrFormat = StringFormat
DotStruct = to_dot_format
UndotStruct = from_dot_format
pprint = pretty_print
cprint = conditional_print
ppath = get_path

if __name__ == '__main__':
    pass
else:
    __all__ = [
        # Functions
        'get_path',
        'pretty_print',
        'conditional_print',
        'find_between',
        'print_debug',
        'map_item',
        'to_dot_format',
        'from_dot_format',
        'nested_list_to_dict',
        'nested_dict_to_list',
        # Aliases
        'DotStruct',
        'UndotStruct',
        'StrFormat',
        'pprint',
        'cprint',
        'ppath',
        # Classes
        'DotDict',
        'DotList',
        'StringFormat',
    ]
