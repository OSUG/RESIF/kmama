from json import loads as json_load
from os.path import isfile
from click import command, option, Path
from kubernetes import client as k8s_client, config as k8s_config
from yaml import dump_all as yaml_dump, Dumper, load_all as yaml_load, Loader
try:
    from kmama.utilities import get_path, DotList, pretty_print, cprint
    from kmama.core.generic import DRYRUN, VERBOSITY
except ImportError:
    from sys import path
    from os import path as P
    path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from utilities import get_path, DotList, pretty_print, cprint
    from core.generic import DRYRUN, VERBOSITY


def get_pvc_from_cluster(kube_config:str, context:str, verbosity:int):
    """https://github.com/kubernetes-client/python/tree/master/kubernetes"""
    cprint(f"DEBUG: List PVC from {context!r} namespace", verbosity > VERBOSITY.INFO)
    try:
        k8s_config.load_kube_config(config_file=kube_config)
        return k8s_client.CoreV1Api().list_namespaced_persistent_volume_claim(namespace=context)
    except (
        k8s_config.config_exception.ConfigException,
        k8s_client.exceptions.ApiException,
    ) as error:
        cprint("ERROR: Impossible to access the cluster with this kubeconfig.")
        message = json_load(error.body)
        cprint("  >>  Status  :", message['status'])
        cprint("  >>  Code    :", message['code'])
        cprint("  >>  Reason  :", message['reason'])
        cprint("  >>  Message :", message['message'])
        exit(1)


def list_volumes_from_pvc(pvc_list:list, verbosity:int):
    """Fill a list of pv/pvc in yaml format to be inserted in the deployment"""
    cprint("DEBUG: Build mounting points for PV/PVC from the retrieved PVC list", verbosity > VERBOSITY.INFO)
    # Init
    patterns = {"resifDynamicVolumes": "data-bynet"}
    archive_volume_mounts, archive_volumes = [], []
    # For each pvc recovered previously
    for pvc in pvc_list.items:
        if not pvc.metadata.labels:
            cprint(f"DEBUG: ... {pvc.metadata.name} ignored (no labels)", verbosity > VERBOSITY.WARNING)
            continue
        labels = pvc.metadata.labels
        for key, value in patterns.items():
            # If there is a label corresponding to one of the patterns
            if key in labels.keys() and labels[key] == value:
                name = f"vol-{labels['netcode']}"
                mount_path = labels['netcode'].upper()
                # Add an element to the yaml list
                archive_volume_mounts.append({
                    "name": name,
                    "mountPath": f"/mnt/auto/archive/data/bynet/{mount_path}",
                    "readOnly": True,
                })
                archive_volumes.append({
                    "name": name,
                    "persistentVolumeClaim": {
                        "claimName": f"{pvc.metadata.name}",
                    },
                })
                cprint(f"DEBUG: ... {pvc.metadata.name} done", verbosity > VERBOSITY.WARNING)
                break
        else:
            cprint(f"DEBUG: ... {pvc.metadata.name} ignored (no match)", verbosity > VERBOSITY.WARNING)
    return archive_volume_mounts, archive_volumes


def update_deployment(deployment_yaml:str, volume_mounts:list, volumes:list, verbosity:int):
    """Reads the provided deployment and completes it by inserting the collected volumes"""
    cprint("DEBUG: Add previous mounting points to deployment.yaml", verbosity > VERBOSITY.INFO)
    # generate a clean yaml
    Dumper.ignore_aliases = lambda *args : True
    # load deployment.yaml
    with open(deployment_yaml, 'r', encoding='utf-8') as f_read:
        deployment_list = DotList(yaml_load(f_read, Loader=Loader))
        for deployment in deployment_list:
            # add volumes
            for container in deployment.spec.template.spec.containers:
                container.setdefault("volumeMounts", []).extend(volume_mounts)
            deployment.spec.template.spec.setdefault("volumes", []).extend(volumes)
        # re-write deployment.yaml
        with open(deployment_yaml, 'w', encoding='utf-8') as f_write:
            yaml_dump(deployment_list.to_list(), f_write, Dumper=Dumper)
    cprint("DEBUG: The volumes have been added to the deployment")


def check_settings(kubeconfig:str, deployment_path:str, context:str, verbosity:str):
    """Check if all arguments are valid"""
    # Init
    patterns = {"resifDynamicVolumes": "data-bynet"}
    # Verbosity:
    if not verbosity or not verbosity.isdigit():
        verbosity = 0
    else:
        verbosity = max(int(verbosity), 0)
    cprint("DEBUG: Start initializing the parameters", verbosity > VERBOSITY.INFO)
    # Kubeconfig path
    try:
        kubeconfig = get_path(kubeconfig, check_path=True, from_script_path=False)
    except FileNotFoundError as error:
        exit(f"ERROR: invalid kubeconfig path\n{error}")
    if not isfile(kubeconfig):
        exit(f"ERROR: Kubeconfig {kubeconfig!r} is not a file")
    cprint(f"DEBUG: ... kubeconfig: {kubeconfig!r}", verbosity > VERBOSITY.WARNING)
    # Deployment.yaml path
    try:
        deployment_path = get_path(deployment_path, check_path=True, from_script_path=False)
    except FileNotFoundError as error:
        exit(f"ERROR: invalid deployment.yaml path\n{error}")
    if not isfile(deployment_path):
        exit(f"ERROR: Deployment {deployment_path!r} is not a file")
    cprint(f"DEBUG: ... deployment.yaml: {deployment_path!r}", verbosity > VERBOSITY.WARNING)
    # Deployment.yaml search label
    with open(deployment_path, 'r', encoding='utf-8') as f_read:
        deployment_list = DotList(yaml_load(f_read, Loader=Loader))
        for deployment in deployment_list:
            if deployment.get("metadata") and deployment.metadata.get("labels"):
                labels = deployment.metadata.labels
                label_found = False
                for key, value in patterns.items():
                    if key in labels.keys() and labels[key] == value:
                        label_found = True
                        break
                if label_found:
                    break
        else:
            print("INFORMATION: The label for dynamic volumes has not been found: nothing has been changed")
            exit(0)
    # Context
    if context not in CONTEXTS:
        print(f"This context is not known by the current script. The recognized contexts are {CONTEXTS}")
        context = DRYRUN
    return kubeconfig, deployment_path, context, verbosity


@command()
@option(
    "-k", "--kubeconfig", "kubeconfig", type=Path(exists=False),
    help='Path to cluster Kubeconfig corresponding to the deployment context.')
@option(
    "-d", "--deployment", "deployment_path", type=Path(exists=False),
    help='Path to application deployment to modify.')
@option(
    "-c", "--context", "context",
    help='Indicate the cluster.')
@option(
    '-v', '--verbosity', 'verbosity',
    help="Integer allowing to manage the level of logs")
def main(kubeconfig, deployment_path, context, verbosity):
    # exit((kube_config, deployment_path, context, verbosity))
    kubeconfig, deployment_path, context, verbosity = check_settings(kubeconfig, deployment_path, context, verbosity)
    pvc_list = get_pvc_from_cluster(kubeconfig, context, verbosity)
    volume_mounts, volumes = list_volumes_from_pvc(pvc_list, verbosity)
    if context == DRYRUN:
        cprint("Volumes Mounts:")
        pretty_print(volume_mounts)
        cprint("Volumes:")
        pretty_print(volumes)
    else:
        update_deployment(deployment_path, volume_mounts, volumes, verbosity)


CONTEXTS = ["production", "staging", "dev", DRYRUN]
main()
