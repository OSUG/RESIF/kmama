try:
    from kmama.utilities import DotStruct
except ImportError:
    from sys import path
    from os import path as P
    path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from utilities import DotStruct


class Yaml:
    def __init__(self, config):
        self.config = DotStruct(config)
        self.deployment = DotStruct({
            "apiVersion": "apps/v1",
            "kind": "Deployment",
        })
        self.service = DotStruct({
            "apiVersion": "v1",
            "kind": "Service",
        })
        self.ingress = DotStruct({
            "apiVersion": "networking.k8s.io/v1",
            "kind": "Ingress",
        })
        self.secrets = DotStruct({})
        self._kustomization = DotStruct({
            "apiVersion": "kustomize.config.k8s.io/v1beta1",
            "kind": "Kustomization",
        })
        self.kustomization_root = DotStruct({})
        self.kustomization_base = DotStruct({})
        self.kustomization_context = DotStruct({})
        self.config_properties = {}

    # Write files

    def write_yaml_deployment(self):
        """
        Fill in the dictionary that will be used to build the deployment.yaml file in the base directory
        """
        self.deployment.update({
            "metadata": {
                "name": f"{self.config.name}-deployment",
            },
            "spec": {
                "template": {
                    "spec": {
                        "containers": [],
                    }
                },
            },
        })
        if self.config.volumes:
            self.deployment.metadata.labels = {
                "resifDynamicVolumes": "data-bynet",
            }
        for container in self.config.containers:
            resulting_container = {
                "name": f"{container.name}",
                "image": f"{container.image}",
                "envFrom": [{
                    "configMapRef": {
                        "name": f"{self.config.name}-configmap",
                    },
                }],
            }
            if container.port:
                resulting_container["ports"] = [{"containerPort": container.port}]
            if (container.cpu_request or container.mem_request or container.cpu_limit or container.mem_limit):
                resulting_container["resources"] = self._resources_constraint(container)
            if container.args:
                resulting_container["args"] = container.args
            if container.command:
                resulting_container["command"] = container.command
            self.deployment["spec"]["template"]["spec"]["containers"].append(resulting_container)

    def write_yaml_service(self):
        """
        Fill in the dictionary that will be used to build the service.yaml file in the base directory
        """
        self.service.update({
            "metadata": {
                "name": f"{self.config.name}-service",
            },
            "spec": {
                "type": "ClusterIP",
                "ports": [{
                    "port": self.config.port,
                }],
            },
        })

    def write_yaml_ingress(self):
        """
        Fill in the dictionary that will be used to build the ingress.yaml file in the base directory
        """
        self.ingress.update({
            "metadata": {
                "name": f"{self.config.name}-ingress",
                "annotations": {
                    "nginx.ingress.kubernetes.io/rewrite-target": "/$1",
                }
            },
            "spec": {
                "rules": [{
                    "http": {
                        "paths": [{
                            "pathType": "Prefix",
                            "path": f"{self.config.url}",
                            "backend": {
                                "service": {
                                    "name": f"{self.config.name}-service",
                                    "port": {
                                        "number": self.config.port,
                                    },
                                },
                            },
                        }],
                    },
                }],
            },
        })

    def _write_secrets(self, context, keys):
        """
        Fill in the dictionary that will be used to build the secret.ignore file in context directory.
        This secret is only a template that should be created with the sops command.
        """
        self.secrets[context.name] = DotStruct({
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {
                "name": f"{context.name}-{self.config.name}-secrets",
                "namespace": f"{context.name}",
                "labels": {
                    "app": self.config.name,
                    "context": context.name,
                },
            },
            "type": "Opaque",
            "stringData": keys,
        })

    def write_yaml_kustomization_root(self):
        """
        Fill in the dictionary that will be used to build the kustomization.yaml file in the root (deploy) directory
        """
        self.kustomization_root.update(self._kustomization)
        self.kustomization_root["resources"] = []
        for context in self.config.contexts:
            self.kustomization_root["resources"].append(f"./overlays/{context.name}")

    def write_yaml_kustomization_base(self):
        """
        Fill in the dictionary that will be used to build the kustomization.yaml file in the base directory
        """
        self.kustomization_base.update(self._kustomization)
        self.kustomization_base.update({
            "resources": [
                "deployment.yaml",
                "service.yaml",
                "ingress.yaml",
            ]
        })

    def write_yaml_kustomization_contexts(self):
        """
        Fill in the dictionary that will be used to build the kustomization.yaml file in each context.
        This method mainly calls patch methods, but it also calls the method for creating secrets if it detects that it is necessary.
        """
        for context in self.config.contexts:
            resulting_context = (self._kustomization)
            resulting_context.update({
                "resources": [
                    "../../base",
                ],
                "namePrefix": f"{context.name}-",
                "namespace": f"{context.name}",
                "commonLabels":{
                    "app": f"{self.config.name}",
                    "context": f"{context.name}",
                },
                "replicas":[{
                    "name": f"{self.config.name}-deployment",
                    "count": context.replicas,
                }],
                "configMapGenerator":[{
                    "name": f"{self.config.name}-configmap",
                    "env": "config.properties",
                }],
            })
            # Patch the image tag (can also implement the image overload)
            patch_image_tag = self._kustomization_images(context)
            if patch_image_tag:
                resulting_context.update({"images": patch_image_tag})
            # For these patches, there is no existing key.
            # So these are mini yaml's that are listed behind the "patches" key
            patches_list = [
                self._kustomization_ingress_hosts(context),
                self._kustomization_ingress_annotations(context),
                self._kustomization_deployment_resources(context),
                self._kustomization_deployment_secrets(context),
                self._kustomization_deployment_other(context),
            ]
            if any(patches_list):
                patches = [patch for patch in patches_list if patch]
                resulting_context.update({"patches" : patches})
            # WARNING: The following line has an execution time of about 5 sec.
            print(
                "Formatting the configuration file and building the dictionaries can take a few seconds."
                "\nPlease wait a moment or have a cup of coffee..."
                f"\n(Building context: {context.name})"
            )
            self.kustomization_context[context.name] = resulting_context
        print("Configuration completed")

    def write_config_properties(self):
        """
        Build a list of "key=value" corresponding to the environment variables (without secrets) 
        """
        env_list = []
        for context in self.config.contexts:
            resulting_config = ""
            for container in context.containers:
                for env_name, env_value in container.env.items():
                    env_list.append(env_name)
                    resulting_config += f"{env_name}={env_value}\n"
            for container in self.config.containers:
                for env_name, env_value in container.env.items():
                    if env_name not in env_list:
                        resulting_config += f"{env_name}={env_value}\n"
            self.config_properties[context.name] = resulting_config

    # Private sub fonctions : write only a file part

    def _kustomization_images(self, context):
        """Add image tag in context kustomization.yaml"""
        images = []
        for container in context.containers:
            images.append({
                "name": f"{container.image}",
                # "newName": f"{container.newImage}",
                "newTag": f"{container.tag}",
            })
        return images

    def _kustomization_ingress_hosts(self, context):
        """Add context host in context kustomization.yaml"""
        return {
            "patch: |- # Ajout de l hote ws(-staging).resif.fr": [[{
                "op": "add",
                "path": "/spec/rules/0/host",
                "value": f"{context.host}",
            }]],
            "target":{
                "kind": "Ingress",
                "name": f"{self.config.name}-ingress",
            }
        } if context.host else None

    def _kustomization_ingress_annotations(self, context):
        """Add context annotations in context kustomization.yaml"""
        return {
            "patch: |- # Ajout d annotations pour l ingress": {
                "apiVersion": "networking.k8s.io/v1",
                "kind": "Ingress",
                "metadata":{
                    "name": f"{self.config.name}-ingress",
                    "annotations": context.annotations
                },
            }
        } if context.annotations else None

    def _kustomization_deployment_resources(self, context):
        """Add resources constraint in context kustomization.yaml"""
        resources = []
        for container in context.containers:
            if (
                container.cpu_request or container.mem_request or
                container.cpu_limit or container.mem_limit
            ):
                resources.append({
                    "name": f"{container.name}",
                    "resources": self._resources_constraint(container),
                })
        return {
            "patch: |- # Ajoute les limites de ressources allouees": {
                "apiVersion": "apps/v1",
                "kind": "Deployment",
                "metadata": {
                    "name": f"{self.config.name}-deployment",
                },
                "spec":{
                    "template":{
                        "spec":{
                            "containers": resources,
                        }
                    }
                }
            }
        } if resources else None

    def _kustomization_deployment_secrets(self, context):
        """Add secrets env in context kustomization.yaml"""
        secrets = []
        secret_keys = {}
        for container in context.containers:
            secrets_by_container = []
            for env_name, secret_key in container.secrets.items():
                secrets_by_container.append({
                    "name": env_name,
                    "valueFrom": {
                        "secretKeyRef": {
                            "name": f"{context.name}-{self.config.name}-secrets",
                            "key": secret_key,
                        }
                    }
                })
                secret_keys.update({secret_key: "TODO"})
            if secrets_by_container:
                secrets.append({
                    "name": f"{container.name}",
                    "env": secrets_by_container,
                })
                self._write_secrets(context, secret_keys)
        return {
                "patch: |- # Ajout des secrets comme variables d environnement": {
                    "apiVersion": "apps/v1",
                    "kind": "Deployment",
                    "metadata": {
                        "name": f"{self.config.name}-deployment",
                    },
                    "spec": {
                        "template": {
                            "spec": {
                                "containers": secrets,
                            }
                        }
                    }
                }
            } if secrets else None

    def _kustomization_deployment_other(self, context):
        """Add commands, args, ports in context kustomization.yaml"""
        patch = []
        for container in context.containers:
            container_patch = {}
            if container.command:
                container_patch["command"] = container.command
            if container.args:
                container_patch["args"] = container.args
            if container.port:
                container_patch["ports"] = [{
                    "containerPort": container.port,
                }]
            if container_patch:
                container_patch["name"] = f"{container.name}"
                patch.append(container_patch)
        return {
                "patch: |- # Ajout des command et args": {
                    "apiVersion": "apps/v1",
                    "kind": "Deployment",
                    "metadata": {
                        "name": f"{self.config.name}-deployment",
                    },
                    "spec": {
                        "template": {
                            "spec": {
                                "containers": patch,
                            }
                        }
                    }
                }
            } if patch else None

    def _resources_constraint(self, container):
        """Build a dictionary with resource constraints to add"""
        resources = {}
        if container.cpu_request or container.mem_request:
            resources["requests"] = {}
            if container.cpu_request:
                resources["requests"]["cpu"] = container.cpu_request
            if container.mem_request:
                resources["requests"]["memory"] = container.mem_request
        if container.cpu_limit or container.mem_limit:
            resources["limits"] = {}
            if container.cpu_limit:
                resources["limits"]["cpu"] = container.cpu_limit
            if container.mem_limit:
                resources["limits"]["memory"] = container.mem_limit
        return resources
