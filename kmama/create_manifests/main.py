from os import path as P
from click import command, option, Path
from yaml import safe_load as yaml_load
try:
    from kmama.utilities import DotStruct, get_path, pretty_print, cprint
    from kmama.create_manifests.format_config import format_config
    from kmama.create_manifests.build_manifest import Yaml
    from kmama.create_manifests.create_manifest import build
except ImportError:
    from sys import path
    from os import path as P
    path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from utilities import DotStruct, get_path, pretty_print, cprint
    from create_manifests.format_config import format_config
    from create_manifests.build_manifest import Yaml
    from create_manifests.create_manifest import build

def check_settings(local_path, app_config):
    """Check if all arguments are valid"""
    # Local path
    try:
        local_path = get_path(local_path, check_path=True, from_script_path=False)
    except FileNotFoundError:
        exit(f"ERROR: non existing path\n{local_path}")
    if not P.isdir(local_path):
        exit(f"ERROR: Path {local_path!r} should be a dir")
    # App config.yaml path
    try:
        app_config = get_path(app_config, check_path=True, from_script_path=False)
    except FileNotFoundError:
        exit(f"ERROR: non existing config.yaml path\n{app_config}")
    if not P.isfile(app_config):
        exit(f"ERROR: Config {app_config!r} should be a file")
    return local_path, app_config


@command()
@option(
    "-p", "--path", "local_path", type=Path(exists=False),
    help='Path to local directory where the project tree will be created.')
@option(
    "-c", "--config", "app_config", type=Path(exists=False),
    help='Path to file containing the application configuration.')
def main(local_path, app_config):
    # exit((local_path, app_config))
    local_path, app_config = check_settings(local_path, app_config)
    with open(app_config, 'r', encoding='utf-8') as file:
        # Load config file
        try:
            config = DotStruct(format_config(yaml_load(file)))
            # pretty_print(config.to_dict())
        except AssertionError:
            exit(f"ERROR: invalid configuration file ({app_config})")
        # Build dictionnaries and corresponding files
        files = Yaml(config)
        files.write_yaml_kustomization_root()
        files.write_yaml_kustomization_base()
        files.write_yaml_deployment()
        files.write_yaml_ingress()
        files.write_yaml_service()
        files.write_yaml_kustomization_contexts()
        files.write_config_properties()
        build(config, files, local_path)
        cprint(f"SUCCES: {config.name} tree created")


if __name__ == "__main__":
    main()
