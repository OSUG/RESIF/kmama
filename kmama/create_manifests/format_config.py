def set_value(obj, key, default_value, result_type):
    """Sets default value if key doesn't exist and checks that the final value is of the expected type"""
    result = obj.get(key, default_value)
    assert isinstance(result, result_type)
    return result


def format_config(dict_config):
    """Format the root app config.yaml"""
    # BASE
    if dict_config is None:
        dict_config = {}
    dict_config["name"] = set_value(dict_config, "name", "__TODO_:_APP_NAME__", str)
    dict_config["port"] = set_value(dict_config, "port", "__TODO_:_SERVICE_PORT__", (int, str))
    dict_config["url"] = set_value(dict_config, "url", "__TODO_:_PATH_URL__", str)
    dict_config["volumes"] = set_value(dict_config, "volumes", False, bool)
    container_settings = { # key: [default_value, result_type]
        "name": [dict_config["name"], str],
        "port": [0, (int, str)],
        "cpu_request": ["", (int, str)],
        "mem_request": ["", (int, str)],
        "cpu_limit": ["", (int, str)],
        "mem_limit": ["", (int, str)],
        "env": [{}, dict],
        "args": [[], (list, str)],
        "command": [[], (list, str)],
        "image": ["__TODO_:_IMAGE__", str],
        "tag": ["__TODO_:_TAG__", (int, str)],
    }
    # BASE CONTAINERS
    dict_config["containers"] = set_value(dict_config, "containers", [], list)
    for b_container in dict_config["containers"]:
        assert isinstance(b_container, dict)
        for key, [default_value, result_type] in container_settings.items():
            b_container[key] = set_value(b_container, key, default_value, result_type)
    # CONTEXTS
    dict_config["contexts"] = set_value(dict_config, "contexts", [{"name": "production"}, {"name": "staging"}], list)
    container_settings.pop("tag")
    container_settings.pop("image")
    container_settings["secrets"] = [{}, dict]
    for context in dict_config["contexts"]:
        assert isinstance(context, dict)
        context["name"] = set_value(context, "name", "__TODO_:_CONTEXT_NAME__", str)
        context["replicas"] = set_value(context, "replicas", 1, (int, str))
        context["host"] = set_value(context, "host", "", str)
        context["annotations"] = set_value(context, "annotations", {}, dict)
        # CONTEXTS CONTAINERS
        context["containers"] = set_value(context, "containers", [], list)
        list_context_containers = []
        for c_container in context["containers"]:
            assert isinstance(c_container, dict)
            for key, [default_value, result_type] in container_settings.items():
                c_container[key] = set_value(c_container, key, default_value, result_type)
            # Image/Tag if context container in base containers
            for base_container in dict_config["containers"]:
                if c_container["name"] == base_container["name"]:
                    c_container["image"] = base_container["image"]
                    c_container["tag"] = set_value(c_container, "tag", base_container["tag"], (int, str))
                    break
            else: # Image/Tag else
                c_container["image"] = set_value(c_container, "image", "__TODO_:_IMAGE__", str)
                c_container["tag"] = set_value(c_container, "tag", "__TODO_:_TAG__", (int, str))
            # List all context containers
            list_context_containers.append(c_container["name"])
        # Adds the containers from the base to the context if they are not already there
        for b_container in dict_config["containers"]:
            if b_container["name"] not in list_context_containers:
                container = {"name": b_container["name"]}
                for key, [default_value, result_type] in container_settings.items():
                    container[key] = set_value(container, key, default_value, result_type)
                container["image"] = b_container["image"]
                container["tag"] = set_value(container, "tag", b_container["tag"], (int, str))
                context["containers"].append(container)
    return dict_config
