from os import mkdir, path as P
from shutil import rmtree
from yaml import dump as yaml_dump
try:
    from kmama.utilities import cprint
    from kmama.core.generic import VERBOSITY
except ImportError:
    from sys import path
    path.insert(1, P.normpath(P.join(P.dirname(P.abspath(__file__)), "..")))
    from utilities import cprint
    from core.generic import VERBOSITY


def existing_directory(directory):
    """
    Request and re-request before deleting the application directory if it already exists
    """
    if P.exists(directory):
        cprint(f"Directory '{directory}' already exists.")
        choice = input("Overwrite its contents ? ([Y]es/[N]o)\n").lower()
        if choice in ("o", "y", "oui", "yes"):
            cprint("Its content will be definitively replaced.")
            choice = input("Confirm ? ([Y]es/[N]o)\n").lower()
            if choice in ("o", "y", "oui", "yes"):
                rmtree(directory)
            else:
                cprint("Cancel.")
                return True
        else:
            cprint("Cancel.")
            return True
    return False


def patch_kustomization(kustomization_path):
    """
    After creating the tree of yaml files of the application,
    - reopen the kustomization files of the different contexts,
    - correct the indentation
    """
    with open(P.join(kustomization_path, "kustomization.yaml"), 'r', encoding='utf-8') as kustomize_r:
        content = kustomize_r.readlines()
        for index, line in enumerate(content):
            content[index] = line.replace("- 'patch: |-", "- patch: |-").replace("- - op:", "  - op:")
    with open(P.join(kustomization_path, "kustomization.yaml"), 'w', encoding='utf-8') as kustomize_w:
        kustomize_w.writelines(content)


def write(file_name, file_content, file_path):
    """Write dictionnary in files via yaml.dump/f.write"""
    with open(P.join(file_path, file_name), 'w', encoding='utf-8') as file_w:
        if file_name.lower().endswith((".yml", ".yaml", "ignore")):
            yaml_dump(file_content.to_dict(), file_w)
        else:
            file_w.write(file_content)


def build(app_config, app_files, local_path):
    """Build tree directories and call write-files functions"""
    app_contexts = []
    for context in app_config.contexts:
        app_contexts.append(context.name)
    root_directory = P.join(local_path, "deploy")
    if existing_directory(root_directory):
        exit(0)
    mkdir(root_directory)
    cprint(f"{root_directory} created")
    write("kustomization.yaml", app_files.kustomization_root, root_directory)
    base = P.join(root_directory, "base")
    mkdir(base)
    cprint(f"{base} created")
    write("kustomization.yaml", app_files.kustomization_base, base)
    write("deployment.yaml", app_files.deployment, base)
    write("ingress.yaml", app_files.ingress, base)
    write("service.yaml", app_files.service, base)
    overlays = P.join(root_directory, "overlays")
    mkdir(overlays)
    for context in app_contexts:
        context_path = P.join(overlays, context)
        mkdir(context_path)
        cprint(f"{context_path} created")
        if app_files.secrets.get(context):
            write("secrets.ignore", app_files.secrets[context], context_path)
        write("kustomization.yaml", app_files.kustomization_context[context], context_path)
        patch_kustomization(context_path)
        write("config.properties", app_files.config_properties[context], context_path)
