git # Kubernetes Manifest Manager (KMAMA)

## Installation du package

> **Prérequis :** installer pipenv

```sh
# Récupérer le dépot du projet
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:OSUG/RESIF/kmama.git
cd kmama
# git checkout kmama
# Activer un environnement virtuel et installer kmama
pipenv install -e .
```

Les répertoires impactés par l'utilisation de l'outil sont :

- le répertoire des sources où est cloné le dépôt kmama ;
- le répertoire de travail (par défaut `./work`) ;
- le répertoire de configuration (`~/config/kmama`).

## Prérequis sops

Il faut d'abord installer l'outil `sops` (https://github.com/mozilla/sops) puis ajouter dans GPG la clé privée mise à disposition dans le coffre fort Résif (https://wiki.osug.fr/!isterre-geodata/resif/equipe/partage_de_secrets). 

Pour cela, copier le bloc suivant dans un fichier local `cle_sops.pgp` et faire ensuite `gpg --import cle_sops.pgp`.

``` 
-----BEGIN PGP PRIVATE KEY BLOCK-----
blablabla
-----END PGP PRIVATE KEY BLOCK-----
```

## Personnalisation

Toutes les valeurs par défaut sont présentes dans des fichiers de configuration au format `.yaml`. Elles peuvent être surchargées en rajoutant vos propres configurations dans le répertoire `~/config/kmama`. Certains paramètres peuvent également être spécifiés sur la ligne de commande auquel cas ils auront la priorité sur les configurations `.yaml`.

On notera principalement les changements de répertoires de travail (où se trouvent les dépôts git des différentes applications).

- modification permanente dans un fichier de configuration

```yaml
# ~/.config/kmama/mon_patch_path.yaml
constants:
  work_directory: ./mon/chemin/vers/mes/depots/git
``` 

- modification temporaire en ligne de commande `--path ./mon/chemin/vers/mes/depots/git`

Une autre personnalisation importante est le chemin vers les kubeconfigs des différents clusters.

- modification permanente dans un fichier de configuration

```yaml
# ~/.config/kmama/mon_patch_kubeconfig.yaml
contexts:
  staging:
    kubeconfig: ./mon/chemin/vers/mon/kubeconfig/staging.yml
  dev:
    kubeconfig: ./mon/chemin/vers/mon/kubeconfig/dev.yml
  production:
    kubeconfig: ./mon/chemin/vers/mon/kubeconfig/production.yml
``` 

- modification temporaire en ligne de commande `--kubeconfig ./mon/chemin/vers/mon/kubeconfig`

Pour les autres changements, vous pouvez trouver plus de détails sur le fonctionnement des fichiers de configuration dans la partie 4 de `./presentation/slides.pdf`.

## Commandes

Le contexte utilisé doit toujours être spécifié en ligne de commande, autrement le contexte `dryrun` est utilisé.

```sh
# Arborescence du projet
tree -I '.git|kmama.egg-info|__pycache__|*.pyc' -a
kmama

# Création et déploiement des volumes correspondants aux réseaux et au contexte spécifié
kmama volumes -c production
kmama volumes --context production

# Déploiement de tous les services (en parallele)
kmama runner -c production -AP
kmama runner --context production --all --parallel

# Test de tous les services (en parallele)
kmama check -c production -AP
kmama check --context production --all --parallel
```

