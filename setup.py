#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path as P
from setuptools import setup, find_packages

current_path = P.dirname(P.abspath(__file__))
with open(P.join(current_path, 'kmama', 'requirements.txt'), 'r', encoding="utf-8") as file:
    required = file.read().splitlines()

setup(
    name='kmama',
    version='1.0.1',
    author='Thibault ROLLAND',
    author_email='Thibault.Rolland@univ-grenoble-alpes.fr',
    license='GPL v3',
    description='Kubernetes Manifest Manager',
    packages=find_packages(),
    install_requires=required,
    include_package_data=True,
    entry_points={
        'console_scripts': [
            # <name> = [<package>.[<subpackage>.]]<module>[:<object>.<object>]
            'write-manifests=kmama.create_manifests.main:main',
            'write-volumes=kmama.dynamic_volumes.main:main',
            'kmama=kmama.main:cli',
        ]
    }
)
